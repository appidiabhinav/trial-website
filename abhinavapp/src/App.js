/* eslint-disable */
import React from "react"
import Jumbotron from "react-bootstrap/Jumbotron"
import Container from "react-bootstrap/Container"
import bg from "./assets/background/blueprint.JPG"
// import Button from 'react-bootstrap/Button';
import logo from "./logo.svg"
import "./App.css"
import { Col, Row } from "react-bootstrap"

function App() {
	return (
		// <div className="App">
		//   <header className="App-header">
		//     <img src={logo} className="App-logo" alt="logo" />
		//     <p>
		//       Edit <code>src/App.js</code> and save to reload.
		//     </p>
		//     <a
		//       className="App-link"
		//       href="https://reactjs.org"
		//       target="_blank"
		//       rel="noopener noreferrer"
		//     >
		//       Learn React
		//     </a>
		//   </header>
		// </div>
		<Container className='p-3'>
			<Jumbotron
				style={{
					color: "white",
				}}>
				<Container className='Top'>
					<header className='header'>
						<a data-scroll='' href='#home' className='header-logo'>
							<img className='header-logo' src={logo} alt='logo' />
						</a>
						<Container className='header-container'>
							<Row>
								<Col md={12}>
									<div className='header-content'>
										<span className='header-tagline'>yfed.finance</span>
										<a
											href='https://www.google.com'
											className='header-btn modal-btn'
											rel='noopener noreferrer'
											target='_blank'>
											<span>Buy YFED</span>
										</a>
									</div>
								</Col>
							</Row>
						</Container>
					</header>
					<section
						className='home section-bg'
						style={{
							background: `url(${bg})  center center / cover no-repeat`,
							height: "100vh",
							width: "100vw",
							margin: 0,
						}}>
						<div className='HomeText' md={12}>
							<div className='home-content'>
								<h1 className='home-title'>Secure &amp; Easy Way To Trade</h1>

								<p className='home-text'>How to start with YFed Finance?</p>

								<div className='home-btns'>
									<a
										data-scroll=''
										href='https://uniswap.info/token/0x2dbd330bc9b7f3a822a9173ab52172bdddcace2a'
										target='_blank'
										className='home-btn home-btn--white'>
										<span>
											<b>Info Token</b>
										</span>
									</a>
								</div>
							</div>
						</div>
					</section>
				</Container>
        <script>
          function Dev(){
            console.log("Made for Abhinav by Priyav K Kaneria")
          }
        </script>
				<Container className='Bottom'>
					<section id='roadmap' className='roadmapsection'>
						<Container>
							<Row>
								<div xl={12} className='title'>
									<h2 className='section-title'>Roadmap</h2>
									<span className='section-tagline'></span>
								</div>
								<div
									md={{ span: 10, offset: 1 }}
									lg={{ span: 6, offset: 3 }}
									xl={{ span: 12, offset: 0 }}>
									<ul className='roadmap'>
										<li className='active'>
											<span>Q2 2020</span>
											<p>
												<img
													draggable='false'
													className='emoji'
													alt='🟢'
													src='https://s.w.org/images/core/emoji/13.0.0/svg/1f7e2.svg'
												/>{" "}
												Project start-up.
											</p>
										</li>
										<li className='active'>
											<span>Q4 2020</span>
											<p>
												<img
													draggable='false'
													className='emoji'
													alt='🟢'
													src='https://s.w.org/images/core/emoji/13.0.0/svg/1f7e2.svg'
												/>{" "}
												Contract Deployment.
											</p>
											<p>
												<img
													draggable='false'
													className='emoji'
													alt='🟢'
													src='https://s.w.org/images/core/emoji/13.0.0/svg/1f7e2.svg'
												/>{" "}
												Website Launch.
											</p>
											<p>
												<img
													draggable='false'
													className='emoji'
													alt='🟢'
													src='https://s.w.org/images/core/emoji/13.0.0/svg/1f7e2.svg'
												/>{" "}
												Listing Uniswap
											</p>
											<p>
												<img
													draggable='false'
													className='emoji'
													alt='🟢'
													src='https://s.w.org/images/core/emoji/13.0.0/svg/1f7e2.svg'
												/>{" "}
												Liquidity Locked One Year.
											</p>
											<p>
												<img
													draggable='false'
													className='emoji'
													alt='🟢'
													src='https://s.w.org/images/core/emoji/13.0.0/svg/1f7e2.svg'
												/>{" "}
												Update Logo Trust Wallet.
											</p>
											<p>
												<img
													draggable='false'
													className='emoji'
													alt='🟢'
													src='https://s.w.org/images/core/emoji/13.0.0/svg/1f7e2.svg'
												/>{" "}
												Listing Bitcratic Exchange.
											</p>
											<p>
												<img
													draggable='false'
													className='emoji'
													alt='🟢'
													src='https://s.w.org/images/core/emoji/13.0.0/svg/1f7e2.svg'
												/>{" "}
												Etherscan Profile Update.
											</p>
											<p>
												<img
													draggable='false'
													className='emoji'
													alt='🟢'
													src='https://s.w.org/images/core/emoji/13.0.0/svg/1f7e2.svg'
												/>{" "}
												Listing Coingecko.
											</p>
											<p>
												<img
													draggable='false'
													className='emoji'
													alt='🔴'
													src='https://s.w.org/images/core/emoji/13.0.0/svg/1f534.svg'
												/>{" "}
												Whitepaper Release.
											</p>
											<p>
												<img
													draggable='false'
													className='emoji'
													alt='🔴'
													src='https://s.w.org/images/core/emoji/13.0.0/svg/1f534.svg'
												/>{" "}
												Yield Staking.
											</p>
											<p>
												<img
													draggable='false'
													className='emoji'
													alt='🔴'
													src='https://s.w.org/images/core/emoji/13.0.0/svg/1f534.svg'
												/>{" "}
												Listing Hotbit Exchange.
											</p>
										</li>
										<li>
											<span>Q1 2021</span>
											<p>
												<img
													draggable='false'
													className='emoji'
													alt='🔴'
													src='https://s.w.org/images/core/emoji/13.0.0/svg/1f534.svg'
												/>{" "}
												Mobile Application.
											</p>
											<p>
												<img
													draggable='false'
													className='emoji'
													alt='🔴'
													src='https://s.w.org/images/core/emoji/13.0.0/svg/1f534.svg'
												/>{" "}
												YFed Swap.
											</p>
											<p>
												<img
													draggable='false'
													className='emoji'
													alt='🔴'
													src='https://s.w.org/images/core/emoji/13.0.0/svg/1f534.svg'
												/>{" "}
												YFed Borrow.
											</p>
										</li>
										<li>
											<span>Q2 2021</span>
											<p>
												<img
													draggable='false'
													className='emoji'
													alt='🔴'
													src='https://s.w.org/images/core/emoji/13.0.0/svg/1f534.svg'
												/>{" "}
												Lending Protocol.
											</p>
											<p>
												<img
													draggable='false'
													className='emoji'
													alt='🔴'
													src='https://s.w.org/images/core/emoji/13.0.0/svg/1f534.svg'
												/>{" "}
												YFed Swipe.
											</p>
										</li>
										<li>
											<span>Q3 2021</span>
											<p>
												<img
													draggable='false'
													className='emoji'
													alt='🔴'
													src='https://s.w.org/images/core/emoji/13.0.0/svg/1f534.svg'
												/>{" "}
												To be Continued...
											</p>
										</li>
									</ul>
								</div>
							</Row>
						</Container>
					</section>
					<footer id='footer' className='footer'>
						<Container>
							<Row>
								<div className='footerdiv' xl={12}>
									<ul className='footer-social'>
										<li>
											<a
												href='https://t.me/YFEDGROUP'
												target='_blank'
												className='ld'>
												<svg
													xmlns='http://www.w3.org/2000/svg'
													viewBox='0 0 240 240'>
													<path
														d='M66.964 134.874s-32.08-10.062-51.344-16.002c-17.542-6.693-1.57-14.928 6.015-17.59 7.585-2.66 186.38-71.948 194.94-75.233 8.94-4.147 19.884-.35 14.767 18.656-4.416 20.407-30.166 142.874-33.827 158.812-3.66 15.937-18.447 6.844-18.447 6.844l-83.21-61.442z'
														fill='none'
														stroke='#409edc'
														strokeWidth='10'></path>
													<path
														d='M92.412 201.62s4.295.56 8.83-3.702c4.536-4.26 26.303-25.603 26.303-25.603'
														fill='none'
														stroke='#409edc'
														strokeWidth='10'></path>
													<path
														d='M66.985 134.887l28.922 14.082-3.488 52.65s-4.928.843-6.25-3.613c-1.323-4.455-19.185-63.12-19.185-63.12z'
														fillRule='evenodd'
														stroke='#409edc'
														strokeWidth='10'
														strokeLinejoin='bevel'></path>
													<path
														d='M66.985 134.887s127.637-77.45 120.09-71.138c-7.55 6.312-91.168 85.22-91.168 85.22z'
														fillRule='evenodd'
														stroke='#409edc'
														strokeWidth='9.67'
														strokeLinejoin='bevel'></path>
												</svg>{" "}
											</a>
										</li>

										<li>
											<a
												href='https://twitter.com/FinanceYfed'
												target='_blank'
												className='tw'>
												<svg
													xmlns='http://www.w3.org/2000/svg'
													width='512'
													height='512'
													viewBox='0 0 512 512'>
													<path d='M496,109.5a201.8,201.8,0,0,1-56.55,15.3,97.51,97.51,0,0,0,43.33-53.6,197.74,197.74,0,0,1-62.56,23.5A99.14,99.14,0,0,0,348.31,64c-54.42,0-98.46,43.4-98.46,96.9a93.21,93.21,0,0,0,2.54,22.1,280.7,280.7,0,0,1-203-101.3A95.69,95.69,0,0,0,36,130.4C36,164,53.53,193.7,80,211.1A97.5,97.5,0,0,1,35.22,199v1.2c0,47,34,86.1,79,95a100.76,100.76,0,0,1-25.94,3.4,94.38,94.38,0,0,1-18.51-1.8c12.51,38.5,48.92,66.5,92.05,67.3A199.59,199.59,0,0,1,39.5,405.6,203,203,0,0,1,16,404.2,278.68,278.68,0,0,0,166.74,448c181.36,0,280.44-147.7,280.44-275.8,0-4.2-.11-8.4-.31-12.5A198.48,198.48,0,0,0,496,109.5Z'></path>
												</svg>
											</a>
										</li>
									</ul>
								</div>
								<div xl={12} className="copyright">
									<small className='footer-copyright'>
										Copyright © 2020 YFed Finance. All rights reserved.
									</small>
								</div>
							</Row>
						</Container>
					</footer>
				</Container>
			</Jumbotron>
		</Container>
	)
}
export default App
